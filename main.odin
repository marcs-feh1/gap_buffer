package gap_buffer

import "core:fmt"

main :: proc(){
}

panic_if :: proc(e: $T){
	if e != nil {
		panic(fmt.tprintf("Error: %v", e))
	}
}
