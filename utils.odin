package gap_buffer

import "core:fmt"

@private
display :: proc(buf: GapBuffer, color := "\e[0;31m"){
	fmt.printf("Gap: %v:%v Raw: ", buf.gap_start, buf.gap_end)
	{
		start := fmt.tprintf("%q", string(buf.data[:buf.gap_start]))
		fmt.print(start[1:len(start) - 1])
	}
	fmt.print(color)
	for _ in 0..<gap_size(buf){
		fmt.print('-')
	}
	fmt.print("\e[0m")
	{
		end := fmt.tprintf("%q", string(buf.data[buf.gap_end:]))
		fmt.print(end[1:len(end) - 1])
	}
	fmt.println("\n--- TEXT ---")

	// for l in buf.lines {
	// }
	fmt.print(string(buf.data[:buf.gap_start]))
	fmt.print(string(buf.data[buf.gap_end:]))

	fmt.println()
}
