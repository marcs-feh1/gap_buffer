package gap_buffer

import ts "core:testing"

@test
resize_gap_test :: proc(t: ^ts.T){
	buf, e := create()
	ts.expect(t, e == nil)
	add_text(&buf, 0, "Hello")
	ts.expect(t, text_len(buf) == 5)

	{
		old_size := gap_size(buf)
		err := resize_gap(&buf, 100)
		new_size := gap_size(buf)

		ts.expect(t, err == nil)
		ts.expect(t, new_size == 100)
		// display(buf)
	}
	{
		old_size := gap_size(buf)
		err := resize_gap(&buf, MIN_GAP)
		new_size := gap_size(buf)

		ts.expect(t, err == nil)
		ts.expect(t, new_size == MIN_GAP)
		// display(buf)
	}
	{
		old_size := gap_size(buf)
		err := resize_gap(&buf, MIN_GAP - 1)
		new_size := gap_size(buf)

		ts.expect(t, err == .BadSize)
		ts.expect(t, new_size == old_size)
		// display(buf)
	}

}

@test
move_gap_test :: proc(t: ^ts.T){
	buf, e := create()
	ts.expect(t, e == nil)
	add_text(&buf, 0, "Hello")
	ts.expect(t, text_len(buf) == 5)

	// display(buf)

	{
		old_size := gap_size(buf)
		err := move_gap(&buf, 0)
		new_size := gap_size(buf)
		ts.expect(t, err == nil)
		ts.expect(t, old_size == new_size)
	}
	{
		old_size := gap_size(buf)
		err := move_gap(&buf, 2)
		new_size := gap_size(buf)
		ts.expect(t, err == .OutOfBounds)
		ts.expect(t, old_size == new_size)
	}
	{
		old_size := gap_size(buf)
		err := move_gap(&buf, gap_size(buf) + 2)
		new_size := gap_size(buf)
		ts.expect(t, err == nil)
		ts.expect(t, old_size == new_size)
	}
	{
		old_size := gap_size(buf)
		err := move_gap(&buf, len(buf.data))
		new_size := gap_size(buf)
		ts.expect(t, err == nil)
		ts.expect(t, old_size == new_size)
	}
	// display(buf)
}

@test
deletion_test :: proc(t: ^ts.T){
	buf, e := create()
	ts.expect(t, e == nil)
	add_text(&buf, 0, "Hellope, World!")
	ts.expect(t, text_len(buf) == 15)

	{
		old_size := gap_size(buf)
		del_text(&buf, 5, 2)
		new_size := gap_size(buf)
		ts.expect(t, new_size - 2 == old_size)
		s, _ := commit_buffer(buf)
		ts.expect(t, s == "Hello, World!")
	}
	{
		old_size := gap_size(buf)
		e = del_text(&buf, text_len(buf) - 1, 1)

		new_size := gap_size(buf)
		ts.expect(t, new_size - 1 == old_size)
		// display(buf)
		s, _ := commit_buffer(buf)
		ts.expect(t, s == "Hello, World")
	}
	{
		old_size := gap_size(buf)
		e = del_text(&buf, 0, 7)

		new_size := gap_size(buf)
		ts.expect(t, new_size - 7 == old_size)
		// display(buf)
		s, _ := commit_buffer(buf)
		ts.expect(t, s == "World")
	}
	{
		old_size := gap_size(buf)
		e = del_text(&buf, 0, 5)

		new_size := gap_size(buf)
		ts.expect(t, new_size - 5 == old_size)
		ts.expect(t, text_len(buf) == 0)
		// display(buf)
		s, _ := commit_buffer(buf)
		ts.expect(t, s == "")
	}
}

/*
	buf, _ := create()
	add_text(&buf, 0, "Hello")
	display(buf)
	fmt.println(text_len(buf), rune_count(buf))

	add_text(&buf, text_len(buf), '世')
	add_text(&buf, text_len(buf), '界')
	fmt.println(text_len(buf), rune_count(buf))

	add_text(&buf, 0, '«')
	add_text(&buf, text_len(buf), '»')
	fmt.println(text_len(buf), rune_count(buf))
*/
