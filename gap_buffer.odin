package gap_buffer

import "core:mem"
import utf "core:unicode/utf8"
// import "core:fmt"

MIN_GAP :: 8

GapBufferError :: enum i8 {
	BadSize,
	OutOfBounds,
}

Error :: union {
	mem.Allocator_Error,
	GapBufferError,
}

GapBuffer :: struct {
	data: []byte,
	gap_start: int,
	gap_end: int,
	lines: [dynamic]int,
	allocator: mem.Allocator,
}

// Create a new gap buffer
create :: proc(allocator := context.allocator) -> (buf: GapBuffer, err: Error) {
	data := make([]byte, MIN_GAP, allocator = allocator) or_return
	lines := make([dynamic]i32, allocator = allocator)

	buf = GapBuffer {
		gap_start = 0,
		gap_end = MIN_GAP,
		data = data,
		lines = lines,
		allocator = allocator,
	}

	return
}

// Resize gap to be `new_size`, may re-allocate buffer
resize_gap :: proc(buf: ^GapBuffer, new_size: int) -> (err: Error) {
	if new_size < MIN_GAP {
		return .BadSize
	}

	total_new_sz := text_len(buf^) + new_size
	new_data := make([]byte, total_new_sz, allocator = buf.allocator) or_return

	{
		before := buf.data[:buf.gap_start]
		after  := buf.data[buf.gap_end:]

		mem.copy_non_overlapping(&new_data[0], raw_data(before), len(before))
		post_offset := min(buf.gap_start + new_size, len(new_data) - 1)
		mem.copy_non_overlapping(&new_data[post_offset], raw_data(after), len(after))
	}

	buf.gap_end = buf.gap_start + new_size

	delete(buf.data, allocator = buf.allocator)
	buf.data = new_data

	return
}

add_text_bytes :: proc(buf: ^GapBuffer, pos: int, data: []byte) -> (err: Error){
	needs_resize := (gap_size(buf^) - len(data)) <= MIN_GAP
	if needs_resize {
		resize_gap(buf, len(data) + (MIN_GAP * 2)) or_return
	}

	off := position_to_buffer_offset(buf^, pos)
	move_gap(buf, off) or_return
	mem.copy_non_overlapping(&buf.data[buf.gap_start], &data[0], len(data))
	buf.gap_start += len(data)
	return
}

add_text :: proc {
	add_text_bytes,
	add_text_string,
	add_text_rune,
}

add_text_string :: proc(buf: ^GapBuffer, pos: int, data: string) -> (err: Error){
	return add_text_bytes(buf, pos, transmute([]byte)data)
}

// Delete a region of size starting at a position (byte offset)
del_text :: proc(buf: ^GapBuffer, pos: int, size: int) -> (err: Error) {
	off := position_to_buffer_offset(buf^, pos)

	if (off + size) > len(buf.data) {
		return .OutOfBounds
	}

	move_gap(buf, off + size) or_return
	buf.gap_start -= size

	assert(buf.gap_start >= 0)
	return
}

// Move gap to byte offset `idx` from start of buffer.
move_gap :: proc(buf: ^GapBuffer, idx: int) -> (err: Error){
	if buf.gap_start == idx || gap_size(buf^) == len(buf.data) { return }
	// NOTE: gap_end can be right outside buffer
	if idx < 0 || idx > len(buf.data) || index_within_gap(buf^, idx) {
		return .OutOfBounds
	}

	if idx < buf.gap_start {
		delta := abs(idx - buf.gap_start)

		src  := &buf.data[idx]
		dest := &buf.data[buf.gap_end - delta]
		mem.copy(dest, src, delta)

		buf.gap_start -= delta
		buf.gap_end -= delta
	}
	else {
		delta := abs(idx - buf.gap_end)

		src  := &buf.data[min(len(buf.data) - 1, buf.gap_end)]
		dest := &buf.data[buf.gap_start]

		mem.copy(dest, src, delta)

		buf.gap_start += delta
		buf.gap_end   += delta
	}

	return
}

// Turn a position (byte offset from text view) to a real position in the buffer
position_to_buffer_offset :: proc(buf: GapBuffer, pos: int) -> int {
	delta := buf.gap_end - buf.gap_start
	raw_pos := pos + (0 if pos < buf.gap_start else delta)

	return raw_pos
}

destroy :: proc(buf: ^GapBuffer){
	delete(buf.data)
	delete(buf.lines)
	buf.gap_end = -1
	buf.gap_start = -1
}

// Size of gap
gap_size :: proc(buf: GapBuffer) -> int {
	return buf.gap_end - buf.gap_start
}

// Length of text, in bytes
text_len :: proc(buf: GapBuffer) -> int {
	return len(buf.data) - (buf.gap_end - buf.gap_start)
}

// Create a string that the buffer represents (gapless)
@require_results
commit_buffer :: proc(buf: GapBuffer, allocator := context.allocator) -> (text: string, err: Error) {
	data := make([]byte, text_len(buf)) or_return
	pre, post := buf.data[:buf.gap_start], buf.data[buf.gap_end:]

	if len(pre) > 0 {
		mem.copy_non_overlapping(&data[0], raw_data(pre), len(pre))
	}
	if len(post) > 0 {
		mem.copy_non_overlapping(&data[len(pre)], raw_data(post), len(post))
	}

	text = string(data)
	return
}

// Scan the entire buffer in search of lines
line_update_global :: proc(buf: ^GapBuffer){
	pre, post := buf.data[:buf.gap_start], buf.data[buf.gap_end:]

	clear(&buf.lines)
	for b, i in pre {
		if b == '\n' {
			append(&buf.lines, i)
		}
	}
	for b, i in post {
		if b == '\n' {
			append(&buf.lines, i + len(pre))
		}
	}
}

get_line_range :: proc(buf: GapBuffer, line: int) -> (int, int){
	begin := buf.lines[line]
	end := text_len(buf) if line + 1 >= len(buf.lines) else buf.lines[line + 1]
	return begin, end
}

// Encode and add rune to byte offset
add_text_rune :: proc(buf: ^GapBuffer, pos: int, r: rune) -> (err: Error){
	bytes, n := utf.encode_rune(r)
	return add_text_bytes(buf, pos, bytes[:n])
}

// Delete rune at byte offset
del_rune :: proc(buf: ^GapBuffer, pos: int) -> (err: Error){
	unimplemented()
}

@(private="file")
index_within_gap :: proc(buf: GapBuffer, idx: int) -> bool {
	return idx >= buf.gap_start && idx < buf.gap_end
}

#assert(size_of(Error) <= size_of(int))
