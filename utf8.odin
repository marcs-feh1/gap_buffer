package gap_buffer

import utf "core:unicode/utf8"

Iterator :: struct {
	current: int,
	buf: ^GapBuffer,
}

iter_next :: proc(it: ^Iterator) -> (rune, bool){
	pos := position_to_buffer_offset(it.buf^, it.current)
	if pos >= len(it.buf.data) {
		return 0, false
	}

	s := it.buf.data[pos:]
	r, n := utf.decode_rune(s)
	if n == 0 {
		return 0, false
	}
	it.current += n

	return r, true
}

next_rune :: proc(buf: GapBuffer, pos: int) -> int {
	off := position_to_buffer_offset(buf, pos)
	if off > len(buf.data){ return pos }

	s := buf.data[off:]
	_, n := utf.decode_rune(s)
	return pos + n
}

previous_rune :: proc(buf: GapBuffer, pos: int) -> int {
	if pos == 0 { return 0 }

	for i := pos - 1; i >= 0; i -= 1 {
		b := byte_at(buf, i)
		if !is_continuation_byte(b){
			_, n := rune_at_offset(buf, i)
			return pos - n
		}
	}

	return 0
}

byte_at :: proc(buf: GapBuffer, pos: int) -> byte {
	off := position_to_buffer_offset(buf, pos)
	return buf.data[off]
}

rune_count :: proc(buf: GapBuffer) -> int {
	buf := buf
	it := Iterator {
		buf = &buf,
	}
	n := 0
	for _ in iter_next(&it){
		n += 1
	}
	return n
}

// Get rune at byte position
rune_at_offset :: proc(buf: GapBuffer, pos: int) -> (rune, int) {
	off := position_to_buffer_offset(buf, pos)
	s := buf.data[off:]
	r, n := utf.decode_rune(s)
	return r, n
}

@(private)
is_continuation_byte :: proc(b: byte) -> bool {
	return b & ~u8(utf.MASKX) == utf.TX
}
