package gap_buffer

Cursor :: struct {
	pos: int,
	mark: int,
}

cursor_move :: proc(buf: GapBuffer, cur: Cursor, delta: int) -> Cursor {
	switch {
	case delta < 0: return cursor_move_left(buf, cur, abs(delta))
	case delta > 0: return cursor_move_right(buf, cur, delta)
	}
	return cur
}

cursor_move_right :: proc(buf: GapBuffer, cur: Cursor, delta: int) -> Cursor {
	pos := cur.pos
	for _ in 0..<delta {
		pos = next_rune(buf, pos)
	}
	return Cursor {
		pos = pos,
		mark = cur.mark,
	}
}

cursor_move_left :: proc(buf: GapBuffer, cur: Cursor, delta: int) -> Cursor {
	pos := cur.pos
	for _ in 0..<delta {
		pos = previous_rune(buf, pos)
	}
	return Cursor {
		pos = pos,
		mark = cur.mark,
	}
}


